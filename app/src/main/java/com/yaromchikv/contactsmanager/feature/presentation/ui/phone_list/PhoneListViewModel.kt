package com.yaromchikv.contactsmanager.feature.presentation.ui.phone_list

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import com.yaromchikv.contactsmanager.feature.domain.usecase.GetContactsUseCase
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.PHONE_PREFERENCE_KEY
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@HiltViewModel
class PhoneListViewModel @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val getContactsUseCase: GetContactsUseCase
) : ViewModel() {

    private val _listOfContacts = MutableStateFlow(emptyList<Contact>())
    val listOfContacts = _listOfContacts.asStateFlow()

    private val _events = MutableSharedFlow<Event>()
    val events = _events.asSharedFlow()

    private var getContactsJob: Job? = null

    init {
        getContacts()
    }

    private fun getContacts() {
        getContactsJob?.cancel()
        getContactsJob = getContactsUseCase()
            .onEach { contacts -> _listOfContacts.value = contacts }
            .launchIn(viewModelScope)
    }

    fun selectPhoneNumberClick(contact: Contact) {
        viewModelScope.launch {
            sharedPreferences.edit().putString(PHONE_PREFERENCE_KEY, contact.phoneNumber).apply()
            _events.emit(Event.SelectPhoneNumber(contact))
        }
    }

    sealed class Event {
        data class SelectPhoneNumber(val contact: Contact) : Event()
    }
}
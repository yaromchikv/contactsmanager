package com.yaromchikv.contactsmanager.feature.domain.usecase

import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import com.yaromchikv.contactsmanager.feature.domain.repository.ContactsRepository
import kotlinx.coroutines.flow.Flow

class GetContactsUseCase(private val repository: ContactsRepository) {
    operator fun invoke(): Flow<List<Contact>> {
        return repository.getContacts()
    }
}
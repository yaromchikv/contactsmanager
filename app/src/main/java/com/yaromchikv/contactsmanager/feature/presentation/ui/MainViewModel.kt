package com.yaromchikv.contactsmanager.feature.presentation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _selectedContact = MutableStateFlow<Contact?>(null)
    val selectedContact = _selectedContact.asStateFlow()

    fun setSelectedContact(contact: Contact) {
        viewModelScope.launch {
            _selectedContact.value = contact
        }
    }
}
package com.yaromchikv.contactsmanager.feature.presentation.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.yaromchikv.contactsmanager.R
import com.yaromchikv.contactsmanager.feature.presentation.ui.actions.ActionsFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ActionsFragment.newInstance())
                .commitNow()
        }
    }
}
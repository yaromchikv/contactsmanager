package com.yaromchikv.contactsmanager.feature.presentation.ui.phone_list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.yaromchikv.contactsmanager.R
import com.yaromchikv.contactsmanager.databinding.DialogPhoneListBinding
import com.yaromchikv.contactsmanager.feature.presentation.ui.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class PhoneListDialogFragment : DialogFragment(R.layout.dialog_phone_list) {

    private val binding by viewBinding(DialogPhoneListBinding::bind)
    private val viewModel by viewModels<PhoneListViewModel>()
    private val activityViewModel by activityViewModels<MainViewModel>()

    private lateinit var phoneListAdapter: PhoneListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        setupCollectors()
    }

    private fun setupRecyclerView() {
        phoneListAdapter = PhoneListAdapter()

        binding.listOfNumbers.apply {
            adapter = phoneListAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        phoneListAdapter.setOnItemClickListener {
            viewModel.selectPhoneNumberClick(it)
        }
    }

    private fun setupCollectors() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.listOfContacts.collectLatest {
                binding.label.text = if (it.isNotEmpty()) getString(R.string.select_number)
                else getString(R.string.select_number_empty)

                phoneListAdapter.submitList(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.events.collectLatest {
                when (it) {
                    is PhoneListViewModel.Event.SelectPhoneNumber -> {
                        activityViewModel.setSelectedContact(it.contact)
                        dismiss()
                    }
                }
            }
        }
    }

}
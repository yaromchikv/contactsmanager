package com.yaromchikv.contactsmanager.feature.presentation.utils

import android.Manifest

object Constants {
    const val PERMISSION = Manifest.permission.READ_CONTACTS
    const val PHONE_PREFERENCE_KEY = "phone_number"
    const val PHONE_LIST_DIALOG_TAG = "phone_list_dialog"

    const val CHANNEL_ID = "notify_channel_id"
    const val CHANNEL_NAME = "Contacts Manager Channel"
    const val NOTIFICATION_ID = 123
}
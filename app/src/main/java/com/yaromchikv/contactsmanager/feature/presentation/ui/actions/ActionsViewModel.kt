package com.yaromchikv.contactsmanager.feature.presentation.ui.actions

import android.content.SharedPreferences
import android.database.Cursor
import android.provider.ContactsContract
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import com.yaromchikv.contactsmanager.feature.domain.usecase.AddContactUseCase
import com.yaromchikv.contactsmanager.feature.domain.usecase.GetContactByPhoneUseCase
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.PHONE_PREFERENCE_KEY
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@HiltViewModel
class ActionsViewModel @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val addContactUseCase: AddContactUseCase,
    private val getContactByPhoneUseCase: GetContactByPhoneUseCase
) : ViewModel() {

    private val _events = MutableSharedFlow<Event>()
    val events = _events.asSharedFlow()

    private val _dialogState = MutableStateFlow<DialogState>(DialogState.NotDisplay)
    val dialogState = _dialogState.asStateFlow()

    fun getContact(id: String, dataCursor: Cursor): Contact {
        var name: String? = null
        var surname: String? = null
        var phone: String? = null
        var email: String? = null

        fun getData(tag: String) = dataCursor.getString(dataCursor.getColumnIndexOrThrow(tag))

        while (dataCursor.moveToNext()) {
            when (getData(ContactsContract.Data.MIMETYPE)) {
                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE -> {
                    name = getData(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)
                    surname =
                        getData(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)
                }
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE -> {
                    if (phone.isNullOrBlank()) {
                        phone = getData(ContactsContract.CommonDataKinds.Phone.NUMBER)
                            .filter { !it.isWhitespace() && it != '-' }
                    }
                }
                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE -> {
                    if (email.isNullOrBlank()) {
                        email = getData(ContactsContract.CommonDataKinds.Email.DATA)
                    }
                }
            }
        }

        return Contact(id.toIntOrNull(), name, surname, phone, email)
    }

    fun saveContact(contact: Contact) {
        viewModelScope.launch {
            if (contact.phoneNumber != null)
                addContactUseCase(contact)
            _events.emit(Event.ShowSaveToast)
        }
    }

    fun selectContactClick() {
        viewModelScope.launch {
            _events.emit(Event.SelectContact)
        }
    }

    fun showPhoneNumbersClick() {
        viewModelScope.launch {
            _events.emit(Event.ShowPhoneNumbersList)
        }
    }

    fun showFromPreferenceClick(emptyText: String) {
        viewModelScope.launch {
            val data = sharedPreferences.getString(PHONE_PREFERENCE_KEY, emptyText) ?: emptyText
            _events.emit(Event.ShowFromPreferences(data))
        }
    }

    fun showInNotificationClick(emptyText: String, notFoundText: String) {
        viewModelScope.launch {
            val phone = sharedPreferences.getString(PHONE_PREFERENCE_KEY, "") ?: ""
            val data =
                if (phone.isEmpty()) emptyText
                else {
                    val contact = getContactByPhoneUseCase(phone).first()
                    if (contact != null)
                        "${contact.name ?: ""} ${contact.surname ?: ""}"
                    else notFoundText
                }
            _events.emit(Event.ShowInNotification(data))
        }
    }

    fun showPermissionDeniedDialog(isTwice: Boolean) {
        viewModelScope.launch {
            _dialogState.value =
                if (!isTwice)
                    DialogState.PermissionDenied
                else
                    DialogState.PermissionDeniedTwice
        }
    }

    fun permissionDialogWasClosed() {
        viewModelScope.launch {
            _dialogState.value = DialogState.NotDisplay
        }
    }

    sealed class Event {
        object SelectContact : Event()
        object ShowSaveToast : Event()
        object ShowPhoneNumbersList : Event()
        data class ShowFromPreferences(val phone: String) : Event()
        data class ShowInNotification(val fullName: String) : Event()
    }

    sealed class DialogState {
        object PermissionDenied : DialogState()
        object PermissionDeniedTwice : DialogState()
        object NotDisplay : DialogState()
    }
}
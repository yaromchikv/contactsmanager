package com.yaromchikv.contactsmanager.feature.data.datasource

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yaromchikv.contactsmanager.feature.domain.model.Contact

@Database(entities = [Contact::class], version = 1, exportSchema = true)
abstract class ContactsDatabase : RoomDatabase() {

    abstract val contactsDao: ContactsDao

    companion object {
        const val DATABASE_NAME = "contacts_db"
    }
}

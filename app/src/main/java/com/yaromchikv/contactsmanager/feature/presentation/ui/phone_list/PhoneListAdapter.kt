package com.yaromchikv.contactsmanager.feature.presentation.ui.phone_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yaromchikv.contactsmanager.databinding.ItemPhoneNumberBinding
import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import timber.log.Timber

class PhoneListAdapter : ListAdapter<Contact, PhoneListAdapter.PhoneViewHolder>(DIFF_CALLBACK) {

    private var onItemClickListener: ((Contact) -> Unit)? = null
    fun setOnItemClickListener(listener: (Contact) -> Unit) {
        onItemClickListener = listener
    }

    inner class PhoneViewHolder(private val binding: ItemPhoneNumberBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(contact: Contact) {
            binding.phoneNumber.text = contact.phoneNumber
            binding.phoneNumber.isSelected = true
            
            itemView.setOnClickListener {
                onItemClickListener?.let { click -> click(contact) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemPhoneNumberBinding.inflate(layoutInflater, parent, false)
        return PhoneViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhoneViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object DIFF_CALLBACK : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.surname == newItem.surname &&
                    oldItem.phoneNumber == newItem.phoneNumber &&
                    oldItem.email == newItem.email
        }
    }
}
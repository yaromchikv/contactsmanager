package com.yaromchikv.contactsmanager.feature.domain.usecase

import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import com.yaromchikv.contactsmanager.feature.domain.repository.ContactsRepository

class AddContactUseCase(private val repository: ContactsRepository) {
    suspend operator fun invoke(contact: Contact) {
        repository.insertContact(contact)
    }
}

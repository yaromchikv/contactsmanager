package com.yaromchikv.contactsmanager.feature.presentation.ui.actions

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.yaromchikv.contactsmanager.R
import com.yaromchikv.contactsmanager.databinding.FragmentActionsBinding
import com.yaromchikv.contactsmanager.feature.presentation.ui.MainViewModel
import com.yaromchikv.contactsmanager.feature.presentation.ui.phone_list.PhoneListDialogFragment
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.CHANNEL_ID
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.CHANNEL_NAME
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.NOTIFICATION_ID
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.PERMISSION
import com.yaromchikv.contactsmanager.feature.presentation.utils.Constants.PHONE_LIST_DIALOG_TAG
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import timber.log.Timber

@AndroidEntryPoint
class ActionsFragment : Fragment(R.layout.fragment_actions) {

    private val binding by viewBinding(FragmentActionsBinding::bind)
    private val viewModel by viewModels<ActionsViewModel>()
    private val activityViewModel by activityViewModels<MainViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            selectContact.setOnClickListener {
                viewModel.selectContactClick()
            }
            showContacts.setOnClickListener {
                viewModel.showPhoneNumbersClick()
            }
            showFromPreference.setOnClickListener {
                viewModel.showFromPreferenceClick(emptyText = getString(R.string.prefs_has_no_phone))
            }
            showInNotification.setOnClickListener {
                viewModel.showInNotificationClick(
                    emptyText = getString(R.string.prefs_has_no_phone),
                    notFoundText = getString(R.string.phone_not_found)
                )
            }
        }

        setupCollectors()
    }

    private fun setupCollectors() {
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            activityViewModel.selectedContact.collectLatest {
                Timber.i("Contact: $it")
                if (it != null) {
                    val fullName = "${it.name ?: ""} ${it.surname ?: ""}"
                    val phoneNumber =
                        if (it.phoneNumber.isNullOrBlank()) getString(R.string.phone_missing) else it.phoneNumber
                    val email =
                        if (it.email.isNullOrBlank()) getString(R.string.email_missing) else it.email

                    with(binding) {
                        contactName.text = fullName
                        contactName.isSelected = true
                        contactPhone.text = phoneNumber
                        contactPhone.isSelected = true
                        contactEmail.text = email
                        contactEmail.isSelected = true
                        contact.visibility = View.VISIBLE
                    }
                } else {
                    binding.contact.visibility = View.INVISIBLE
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.events.collectLatest {
                when (it) {
                    is ActionsViewModel.Event.SelectContact -> {
                        checkPermission.launch(PERMISSION)
                    }
                    is ActionsViewModel.Event.ShowSaveToast -> {
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.saving_successfully),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is ActionsViewModel.Event.ShowPhoneNumbersList -> {
                        PhoneListDialogFragment().show(childFragmentManager, PHONE_LIST_DIALOG_TAG)
                    }
                    is ActionsViewModel.Event.ShowFromPreferences -> {
                        Snackbar.make(binding.root, it.phone, Snackbar.LENGTH_LONG).show()
                    }
                    is ActionsViewModel.Event.ShowInNotification -> {
                        createNotificationChannel()
                        notificationBuilder.setContentText(it.fullName)
                        with(NotificationManagerCompat.from(requireContext())) {
                            notify(NOTIFICATION_ID, notificationBuilder.build())
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.dialogState.collectLatest {
                when (it) {
                    is ActionsViewModel.DialogState.PermissionDenied -> {
                        showAlertDialog(
                            getString(R.string.oops),
                            getString(R.string.permission_denied_message)
                        ) {
                            checkPermission.launch(PERMISSION)
                        }
                    }
                    is ActionsViewModel.DialogState.PermissionDeniedTwice -> {
                        showAlertDialog(
                            getString(R.string.oops),
                            getString(R.string.permission_denied_twice_message)
                        ) {
                            val intent =
                                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                                    data =
                                        Uri.fromParts("package", requireContext().packageName, null)
                                }
                            startActivity(intent)
                        }
                    }
                    else -> Unit
                }
            }
        }
    }

    private var alertDialog: AlertDialog? = null

    private fun showAlertDialog(title: String, message: String, positive: () -> Unit) {
        alertDialog = AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                viewModel.permissionDialogWasClosed()
                positive()
            }
            .setOnCancelListener { viewModel.permissionDialogWasClosed() }
            .create()
        alertDialog?.show()
    }

    private val checkPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                pickContact.launch(null)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale(PERMISSION)) {
                        Timber.d("Permission DENIED")
                        viewModel.showPermissionDeniedDialog(false)
                    } else {
                        Timber.d("Permission DENIED TWICE")
                        viewModel.showPermissionDeniedDialog(true)
                    }
                }
            }
        }

    private val pickContact =
        registerForActivityResult(ActivityResultContracts.PickContact()) { uri ->
            if (uri != null) {
                val contactCursor =
                    requireContext().contentResolver.query(uri, null, null, null, null)
                if (contactCursor?.moveToFirst() == true) {
                    val id: String =
                        contactCursor.getString(contactCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID))
                    val dataCursor = requireContext().contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        null,
                        ContactsContract.Data.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    if (dataCursor != null) {
                        val contact = viewModel.getContact(id, dataCursor)
                        activityViewModel.setSelectedContact(contact)
                        viewModel.saveContact(contact)
                    }
                    dataCursor?.close()
                }
                contactCursor?.close()
            }
        }

    private val notificationBuilder by lazy {
        NotificationCompat.Builder(requireContext(), CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_person)
            .setContentTitle(getString(R.string.app_name))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            val notificationManager =
                requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onStop() {
        super.onStop()
        alertDialog?.dismiss()
    }

    companion object {
        fun newInstance() = ActionsFragment()
    }
}
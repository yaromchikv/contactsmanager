package com.yaromchikv.contactsmanager.feature.domain.repository

import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import kotlinx.coroutines.flow.Flow

interface ContactsRepository {
    fun getContacts(): Flow<List<Contact>>
    fun getContactByPhone(phone: String): Flow<Contact?>
    suspend fun insertContact(contact: Contact)
}
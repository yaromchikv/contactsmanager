package com.yaromchikv.contactsmanager.feature.data.repository

import com.yaromchikv.contactsmanager.feature.data.datasource.ContactsDao
import com.yaromchikv.contactsmanager.feature.domain.model.Contact
import com.yaromchikv.contactsmanager.feature.domain.repository.ContactsRepository
import kotlinx.coroutines.flow.Flow

class ContactsRepositoryImpl(private val dao: ContactsDao) : ContactsRepository {
    override fun getContacts(): Flow<List<Contact>> {
        return dao.getContacts()
    }

    override fun getContactByPhone(phone: String): Flow<Contact?> {
        return dao.getContactByPhone(phone)
    }

    override suspend fun insertContact(contact: Contact) {
        dao.insertContact(contact)
    }
}
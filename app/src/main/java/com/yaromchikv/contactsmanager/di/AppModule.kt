package com.yaromchikv.contactsmanager.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.yaromchikv.contactsmanager.feature.data.datasource.ContactsDatabase
import com.yaromchikv.contactsmanager.feature.data.repository.ContactsRepositoryImpl
import com.yaromchikv.contactsmanager.feature.domain.repository.ContactsRepository
import com.yaromchikv.contactsmanager.feature.domain.usecase.AddContactUseCase
import com.yaromchikv.contactsmanager.feature.domain.usecase.GetContactsUseCase
import com.yaromchikv.contactsmanager.feature.domain.usecase.GetContactByPhoneUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): ContactsDatabase {
        return Room.databaseBuilder(
            context,
            ContactsDatabase::class.java,
            ContactsDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideContactsRepository(db: ContactsDatabase): ContactsRepository =
        ContactsRepositoryImpl(db.contactsDao)

    @Provides
    @Singleton
    fun provideGetContactsUseCase(repository: ContactsRepository): GetContactsUseCase =
        GetContactsUseCase(repository)

    @Provides
    @Singleton
    fun provideGetNameByPhoneUseCase(repository: ContactsRepository): GetContactByPhoneUseCase =
        GetContactByPhoneUseCase(repository)

    @Provides
    @Singleton
    fun provideAddContactUseCase(repository: ContactsRepository): AddContactUseCase =
        AddContactUseCase(repository)
}